export default interface CanvasOptionType {

    /**
     * 是否支持区域管理，默认true
     */
    region?: boolean

}